package de.hs_lu.beans;

import java.sql.Date;

/**
 * Created by Shadow on 03.10.2016.
 */
public class DishStats {
    Date stat_day;
    Double eiweiss;
    Double fett;
    Double kalorien;
    Double kjoule;
    Double kohlehydrate;

    public DishStats(Date stat_day, Double eiweiss, Double fett, Double kalorien, Double kjoule, Double kohlehydrate) {
        super();
        this.stat_day = stat_day;
        this.eiweiss = eiweiss;
        this.fett = fett;
        this.kalorien = kalorien;
        this.kjoule = kjoule;
        this.kohlehydrate = kohlehydrate;
    }

    public String toTableRow() {
        return "<td>" + stat_day + "</td>"
                + "<td>" + eiweiss + "</td>"
                + "<td>" + fett + "</td>"
                + "<td>" + kalorien + "</td>"
                + "<td>" + kjoule + "</td>"
                + "<td>" + kohlehydrate + "</td>";
    }

    public Date getStat_day() {
        return stat_day;
    }

    public void setStat_day(Date stat_day) {
        this.stat_day = stat_day;
    }

    public Double getEiweiss() {
        return eiweiss;
    }

    public void setEiweiss(Double eiweiss) {
        this.eiweiss = eiweiss;
    }

    public Double getFett() {
        return fett;
    }

    public void setFett(Double fett) {
        this.fett = fett;
    }

    public Double getKalorien() {
        return kalorien;
    }

    public void setKalorien(Double kalorien) {
        this.kalorien = kalorien;
    }

    public Double getKjoule() {
        return kjoule;
    }

    public void setKjoule(Double kjoule) {
        this.kjoule = kjoule;
    }

    public Double getKohlehydrate() {
        return kohlehydrate;
    }

    public void setKohlehydrate(Double kohlehydrate) {
        this.kohlehydrate = kohlehydrate;
    }
}
