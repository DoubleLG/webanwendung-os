$(document).ready(function () {
    var table = $('#allFoodsTable').DataTable({
        "pageLength": 5,
        "bLengthChange": false,
        "language": {"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"}
    });

    $('#allFoodsTable tbody').on('click', 'button', function () {
        var data = table.row($(this).parents('tr')).data();
        dishTable.row.add(["<input type='hidden' name='foodID' value='" + data[0] + "'>", data[1], "<input type='number' name='amount' min='1' max='99' value='1'>"]).draw(false);
        // alert('${user.toString()');
        // alert(#{fdb.HTMLFromDBWithButtons});
    });

    var dishTable = $('#dishFoodTable').DataTable({
        "bLengthChange": false,
        "bFilter": false
    });

    // Hier wird das aktuelle Datum als Standard für den Datepicker gesetzt
    var now = new Date();
    var today = now.getFullYear() + '-' + (now.getMonth() + 1) + '-' + now.getDate();
    $('#datepicker').val(today);

    $('#sub_form').submit(function () {

        /*   alert("test: " + dishTable.rows().data());
         console.log(dishTable.rows().data().toArray())*/

    });
    // Hier wird automatisch eine erste leere Zeile eingefügt
    //  $('addDish').click();
});


function addFoodToDish() {
}
