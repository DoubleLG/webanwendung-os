<%@page import="de.hs_lu.beans.UserBean" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html>
<html>
<head>
    <title>Title</title>
</head>
<body>
<jsp:useBean id="user" class="de.hs_lu.beans.UserBean" scope="session"/>
<%-- Hier wird der �bergebene String auf ungleich null gepr�ft--%>
<%! public String denullify(String s) {
    return(s==null)?"":s;
} %>

<%
String logout = this.denullify(request.getParameter("logout"));

    if(logout.equals("logout")) {
        user.setAngemeldet(false);
        response.sendRedirect("./loginView.jsp");
    }
%>



</body>
</html>
