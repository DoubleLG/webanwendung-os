package de.hs_lu.beans;

import java.sql.*;

import de.hs_lu.database.MySQL;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class FoodTableBean {
    Vector<Food> foodListe;

    public FoodTableBean() throws SQLException, ClassNotFoundException {
        super();
        foodListe = new Vector<Food>();
        this.getFoodFromDB();
    }

    public void getFoodFromDB() throws SQLException, ClassNotFoundException {
        String sql = "SELECT ID, Nahrungsmittel, Kcal, Kjoule, Fett, Kohlehydrate, Eiweiß, Rubrik FROM nutrition";

        Connection dbConn = new MySQL().getConnection();

        Statement stmt = dbConn.createStatement();
        ResultSet dbRes = stmt.executeQuery(sql);

        while (dbRes.next()) {
            Food newFood = new Food(
                    dbRes.getString("ID"),
                    dbRes.getString("Nahrungsmittel"),
                    dbRes.getDouble("Kcal"),
                    dbRes.getDouble("Kjoule"),
                    dbRes.getDouble("Fett"),
                    dbRes.getDouble("Kohlehydrate"),
                    dbRes.getDouble("Eiweiß"),
                    dbRes.getString("Rubrik")
            );
            foodListe.add(newFood);
        }
    }

    public String getHTMLFromDB() {
        String html = "";

        for (Food myFood : foodListe) {
            html += "<tr id=" + myFood.getFoodId() +">" + myFood.toTableRow() + "</tr>";
        }
        return html;

    }

    public String getHTMLFromDBWithButtons(){
        String html = "";
        for(Food myFood : foodListe){
            html += "<tr id=" + myFood.getFoodId() +">" + myFood.toTableRow() + "<td><button><span class='glyphicon glyphicon-plus-sign'></span></button></td></tr>";
        }

        return html;
    }

    // Diese Funktion wird nicht gebraucht, da eine Implementierung von 'typeahead' fehlgeschlagen ist
    public List<String> getMatchedFoodData(String query) throws SQLException, ClassNotFoundException {
        List<String> matchedFoods = new ArrayList<String>();
        String sql = "SELECT * FROM nutrition WHERE Nahrungsmittel LIKE ?";

        System.out.println(sql);

        Connection dbConn = new MySQL().getConnection();

        PreparedStatement prep = dbConn.prepareStatement(sql);
        query = "%" + query + "%";
        prep.setString(1, query);

        ResultSet rs = prep.executeQuery();

        while (rs.next()) {
            matchedFoods.add(rs.getString("Nahrungsmittel"));
        }

        return matchedFoods;

    }


    public Vector<Food> getFoodListe() {
        return foodListe;
    }

    public void setFoodListe(Vector<Food> foodListe) {
        this.foodListe = foodListe;
    }
}
