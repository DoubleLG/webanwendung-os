<%@page import="de.hs_lu.beans.UserBean" %>
<%@page import="de.hs_lu.beans.DishBean" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>My Foods</title>
    <!-- Im Folgenden werden die Importe organisiert -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7"
          crossorigin="anonymous">
    <%-- add a new font --%>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald">
    <!--  own customized stylesheet -->
    <link rel="stylesheet" href="../css/ownCustomization.css">


    <!-- Include DataTables via CDN - CSS -->
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/v/dt/jqc-1.12.3/dt-1.10.12/datatables.min.css"/>

</head>

<body>
<jsp:useBean id="user" class="de.hs_lu.beans.UserBean" scope="session"></jsp:useBean>
<jsp:useBean id="dish" class="de.hs_lu.beans.DishBean" scope="session"></jsp:useBean>
<jsp:setProperty name="dish" property="userID" value="<%=user.getIdentifierSQL()%>"/>
<%
    if (!user.isAngemeldet()) {
        response.sendRedirect("./loginView.jsp");
    }
%>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><img src="../images/healthy-food.png"/></a>
            <a class="navbar-brand" href="../index.jsp">FitnessSeite</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="../html/imprint.html" data-toggle="modal"
                       data-target="#imprint">Impressum</a></li>
                <li><a href="../html/privacy.html" data-toggle="modal"
                       data-target="#data_sec">Datenschutz</a></li>
                <li><p class="navbar-btn"><a href="./dashboardAppl.jsp?logout=logout" class="btn btn-default">
                    <span class="glyphicon glyphicon-off"></span> Logout</a>
                </p></li>
            </ul>
            <form class="navbar-form navbar-right">
                <input type="text" class="form-control" placeholder="Search...">
            </form>
        </div>
    </div>
</nav>

<!-- Dashboard Area -->
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <li><a href="./dashboardView.jsp"><span class="glyphicon glyphicon-home"></span>Home</a>
                </li>
                <li><a href="./foodDatabaseView.jsp"><span class="glyphicon glyphicon-list-alt"></span>Nahrungsmittel-DB</a>
                </li>
                <li><a href="myFoodsView.jsp"><span class="glyphicon glyphicon-pencil"></span>Ernährungstagebuch</a>
                </li>
                <li class="active"><a href="myAnalyticsView.jsp"><span class="glyphicon glyphicon-stats"></span>Auswertungen</a></li>
            </ul>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Auswertungen</h1>


            <h2 class="sub-header">Meine Auswertungen</h2>

        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

            <%--Hier wird eine Tabelle mit den Auswertungen der eigenen verzehrten Gerichte erstellt--%>

            <table id="userDishTable" class="display" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Datum</th>
                    <th>Eiweiß</th>
                    <th>Fett</th>
                    <th>Kalorien</th>
                    <th>Kjoule</th>
                    <th>Kohlehydrate</th>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <th>Datum</th>
                    <th>Eiweiß</th>
                    <th>Fett</th>
                    <th>Kalorien</th>
                    <th>Kjoule</th>
                    <th>Kohlehydrate</th>
                </tr>
                </tfoot>

                <tbody>

                <jsp:getProperty name="dish" property="HTMLFromDishTable"/>
                </tbody>

            </table>
        </div>
    </div>

</div>

<div id="container4chart" class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main"></div>

<!-- Ende Dashboard Area -->


<!-- Modal Impressum-->
<div class="modal fade" id="imprint" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="imprint"></div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Modal Datenschutz-->
<div class="modal fade" id="data_sec" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="privacy"></div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="navbar-fixed-bottom">
    <footer>
        &copy; 2016 BW4S - FitnessSeite
    </footer>
</div>
<!-- /container -->

<!-- JS-Files am Ende platziert, damit diese schneller laden -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="http://code.jquery.com/jquery-latest.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
<!-- Include DataTables via CDN - JS -->
<script type="text/javascript"
        src="https://cdn.datatables.net/v/dt/jqc-1.12.3/dt-1.10.12/datatables.min.js"></script>
<%-- Einbinden von HighCharts fuer die Darstellung von Graphen --%>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<!-- Include own JS for DataTable implementation -->
<script type="text/javascript" src="../js/myAnalytics.js"></script>


</body>

</html>

