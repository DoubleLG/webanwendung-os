<%@page import="de.hs_lu.beans.UserBean" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Dashboard</title>
    <!-- Im Folgenden werden die Importe organisiert -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7"
          crossorigin="anonymous">
    <%-- add a new font --%>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald">
    <!--  own customized stylesheet -->
    <link rel="stylesheet" href="../css/ownCustomization.css">

</head>


<body>
<jsp:useBean id="user" class="de.hs_lu.beans.UserBean" scope="session"></jsp:useBean>
<%
    if (!user.isAngemeldet()) {
        response.sendRedirect("./loginView.jsp");
    }
%>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><img src="../images/healthy-food.png"/></a>
            <a class="navbar-brand" href="../index.jsp">FitnessSeite</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="../html/imprint.html" data-toggle="modal"
                       data-target="#imprint">Impressum</a></li>
                <li><a href="../html/privacy.html" data-toggle="modal"
                       data-target="#data_sec">Datenschutz</a></li>
                <li><p class="navbar-btn"><a href="./dashboardAppl.jsp?logout=logout" class="btn btn-default">
                    <span class="glyphicon glyphicon-off"></span> Logout</a>
                </p></li>
            </ul>
            <form class="navbar-form navbar-right">
                <input type="text" class="form-control" placeholder="Search...">
            </form>
        </div>
    </div>
</nav>

<!-- Dashboard Area -->
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <li class="active"><a href="./dashboardView.jsp"><span class="glyphicon glyphicon-home"></span>Home</a>
                </li>
                <li><a href="./foodDatabaseView.jsp"><span class="glyphicon glyphicon-list-alt"></span>Nahrungsmittel-DB</a>
                </li>
                <li><a href="myFoodsView.jsp"><span class="glyphicon glyphicon-pencil"></span>Ernährungstagebuch</a>
                </li>
                <li><a href="myAnalyticsView.jsp"><span class="glyphicon glyphicon-stats"></span>Auswertungen</a></li>
            </ul>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header" align="center">Willkommen auf der FitnessSeite</h1>
            <h2 class="sub-header">Ihr persönliches Ernährungstagebuch!</h2>

            <!-- Three columns of text below the carousel -->
            <div class="row">
                <div class="col-sm-4 col-md-4 col-lg-4">
                    <img class="featurette-image img-responsive center-block"
                         src="../images/openshift.png"
                         alt="Openshift_PaaS">
                    <h2>Openshift Online PaaS</h2>
                    <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh
                        ultricies
                        vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent
                        commodo
                        cursus magna.</p>
                    <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
                </div><!-- /.col-lg-4 -->
                <div class="col-sm-4 col-md-4 col-lg-4">
                    <img class="featurette-image img-responsive center-block"
                         src="../images/mysql_55.png"
                         alt="MySQL">
                    <h2>MySQL 5.5</h2>
                    <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.
                        Cras
                        mattis consectetur purus sit amet fermentum. Fusce dapibus, tellus ac cursus commodo, tortor
                        mauris
                        condimentum nibh.</p>
                    <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
                </div><!-- /.col-lg-4 -->
                <div class="col-sm-4 col-md-4 col-lg-4">
                    <img class="featurette-image img-responsive center-block"
                         src="../images/bootstrap.png"
                         alt="BootstrapFramework">
                    <h2>Bootstrap</h2>
                    <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id
                        ligula
                        porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum
                        nibh, ut
                        fermentum massa justo sit amet risus.</p>
                    <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
                </div><!-- /.col-lg-4 -->
            </div><!-- /.row -->


            <!-- START THE FEATURETTES -->

            <hr class="featurette-divider">

            <div class="row featurette">
                <div class="col-md-7">
                    <h2 class="featurette-heading">First featurette heading. <span
                            class="text-muted">It'll blow your mind.</span></h2>
                    <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis
                        euismod
                        semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus
                        ac cursus
                        commodo.</p>
                </div>
                <div class="col-md-5">
                    <img class="featurette-image img-responsive center-block" src="../images/highcharts.png"
                         alt="Highcharts">
                </div>
            </div>

            <hr class="featurette-divider">

            <div class="row featurette">
                <div class="col-md-7 col-md-push-5">
                    <h2 class="featurette-heading">Oh yeah, it's that good. <span
                            class="text-muted">See for yourself.</span>
                    </h2>
                    <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis
                        euismod
                        semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus
                        ac cursus
                        commodo.</p>
                </div>
                <div class="col-md-5 col-md-pull-7">
                    <img class="featurette-image img-responsive center-block" src="../images/cdn-graph.jpg"
                         alt="CDN_graph">
                </div>
            </div>

            <hr class="featurette-divider">

            <div class="row featurette">
                <div class="col-md-7">
                    <h2 class="featurette-heading">And lastly, this one. <span class="text-muted">Checkmate.</span></h2>
                    <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis
                        euismod
                        semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus
                        ac cursus
                        commodo.</p>
                </div>
                <div class="col-md-5">
                    <img class="featurette-image img-responsive center-block" src="../images/CDN.png"
                         alt="CDN">
                </div>
            </div>

            <hr class="featurette-divider">

            <!-- /END THE FEATURETTES -->

        </div>
    </div>
</div>


</div><!-- /.container -->
<!-- Ende Dashboard Area -->

<!-- Modal Impressum-->
<div class="modal fade" id="imprint" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="imprint"></div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Modal Datenschutz-->
<div class="modal fade" id="data_sec" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="privacy"></div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="navbar-fixed-bottom">
    <footer>
        &copy; 2016 BW4S - FitnessSeite
    </footer>
</div>
<!-- /container -->

<!-- JS-Files am Ende platziert, damit diese schneller laden -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script type="text/javascript"
        src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script
        src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
        integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
        crossorigin="anonymous"></script>

</body>

</html>