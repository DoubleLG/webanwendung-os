/**
 * Created by Shadow on 03.10.2016.
 */

$(document).ready(function () {
    var dishTable = $('#userDishTable').DataTable(
        {"bSort": false,
        "language": {"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"}
        }
    );

    var labelData = []
    var jsonData = []
    for(i=0; i<dishTable.data().length; i++){
        labelData.push(dishTable.data()[i][0]);
        jsonData.push([dishTable.data()[i][0], parseFloat(dishTable.data()[i][3])]);
    }

    Highcharts.chart('container4chart', {
        chart: {
            type: 'spline'
        },
        title: {
            text: 'Kalorienverbrauch in der Vergangenheit'
        },
        subtitle: {
            text: 'Erhobener Verbrauch aus Deinen hinterlegten Speisen'
        },
        xAxis: {

            title: {
                text: 'Datum des Eintrags'
            },
            description: 'Dates',
            categories: labelData
        },
        yAxis: {
            title: {
                text: 'Verbrauchte Kalorien (kcal)'
            },
            min: 0
        },
        // tooltip: {
        //     pointFormat: '{point.x:%e. %b}: {point.y:.2f} m'
        // },

        plotOptions: {
            spline: {
                marker: {
                    enabled: true
                }
            }
        },

        series: [{
            name: 'Kalorienverbrauch',
            data: jsonData
            // data: [[dishTable.data()[0][0], parseFloat(dishTable.data()[0][3])], [dishTable.data()[1][0], parseFloat(dishTable.data()[1][3])]]
            // data:[["2016-01-01", "100"], ["2016-01-12","200"]]
        }]
    });


});