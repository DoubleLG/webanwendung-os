<%@page import="de.hs_lu.beans.UserBean" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>FitnessSeite</title>
    <!-- Im Folgenden werden die Importe organisiert -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7"
          crossorigin="anonymous">
    <%-- add a new font --%>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald">
    <!--  own customized stylesheet -->
    <link rel="stylesheet" href="./css/ownCustomization.css">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script type="text/javascript"
            src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script
            src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
            integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
            crossorigin="anonymous"></script>
</head>
<body>

<jsp:useBean id="user" class="de.hs_lu.beans.UserBean" scope="session"></jsp:useBean>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed"
                    data-toggle="collapse" data-target="#navbar" aria-expanded="false"
                    aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span> <span
                    class="icon-bar"></span> <span class="icon-bar"></span> <span
                    class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><img src="./images/healthy-food.png"/></a>
            <a class="navbar-brand" href="#">FitnessSeite</a>

        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <form class="navbar-form navbar-right" action="./jsp/loginAppl.jsp"
                  method="get">
                <div class="form-group">
                    <input type="text" name="userId" placeholder="Username" class="form-control">
                </div>
                <div class="form-group">
                    <input type="password" name="password" placeholder="Password" class="form-control">
                </div>
                <button type="submit" class="btn btn-success" name="login"
                        value="login">Sign in
                </button>
            </form>
        </div>
        <!--/.navbar-collapse -->
    </div>
</nav>

<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="jumbotron">
    <div class="container">
        <h1 align="center">Herzlich willkommen auf der FitnessSeite!</h1>
        <p>Dieses ist unsere Internetseite f�r Anwendungssysteme 520, welche von
            OpenShift gehosted, eine MySQL-DB einsetzt und das
            Bootstrap-Framework f�r das Design nutzt.</p>
        <p>
            <a class="btn btn-primary btn-lg" href="./jsp/registerView.jsp" role="button">Registrieren</a>
        </p>
    </div>
</div>


<div class="container">

    <div class="row">
        <div id="landingpage-slider" class="carousel slider" data-ride="carousel">
            <!-- Indikator fuer die Punkte in der Navigation -->

            <!-- Wrapper fuer die Slides -->
            <div align="center" class="carousel-inner" role="listbox">
                <div class="item active">
                    <img class="img-responsive img-circle" src="./images/dish1.jpg" alt="dish1"/>
                </div>
                <div class="item">
                    <img class="img-responsive img-circle" src="./images/dish2.jpg" alt="dish2"/>
                </div>
                <div class="item">
                    <img class="img-responsive img-circle" src="./images/tenderloin.jpg" alt="tenderloin"/>
                </div>
            </div>

            <!-- Kontrollfunktionen - Vor- und Zurueck-Buttons -->
            <!-- links -->
            <a class="left carousel-control" href="#landingpage-slider" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            </a>
            <!-- rechts -->
            <a class="right carousel-control" href="#landingpage-slider" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            </a>


        </div>

    </div>

    <hr>

</div>

<div class="navbar-fixed-bottom">
    <footer>
        &copy; 2016 BW4S - FitnessSeite
    </footer>
</div>
<!-- /container -->
</body>
</html>