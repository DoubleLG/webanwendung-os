package de.hs_lu.beans;

import de.hs_lu.database.MySQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Vector;

/**
 * Created by Shadow on 03.10.2016.
 */
public class DishBean {
    Vector<DishStats> auswertung;
    Integer userID;


    public DishBean() throws SQLException, ClassNotFoundException {
        super();
        auswertung = new Vector<DishStats>();
    }

    public void saveDish(String[] foods, String[] amounts, int userId, Date dishTimestamp) throws SQLException, ClassNotFoundException {

        java.sql.Date sqlDate = new java.sql.Date(dishTimestamp.getTime());

        for (int i = 0; i < foods.length; i++) {

            String sql = "INSERT INTO dishes (NUTRITION_ID, USER_ID, AMOUNT, TIMESTAMP) VALUES (?,?,?,?)";

            Connection dbConn = new MySQL().getConnection();

            PreparedStatement prep = dbConn.prepareStatement(sql);

            prep.setInt(1, Integer.parseInt(foods[i]));
            prep.setInt(2, userId);
            prep.setInt(3, Integer.parseInt(amounts[i]));
            prep.setDate(4, sqlDate);

            prep.executeUpdate();
        }
    }

    public void getStats(int userID) throws SQLException, ClassNotFoundException {
        auswertung.clear();
        String sql = "SELECT TIMESTAMP AS stat_day,  Sum(n.Eiweiß * d.amount) AS eiweiß, Sum( n.Fett* d.amount) AS fett, Sum(n.Kcal*d.amount) as kalorien, Sum( n.Kjoule * d.amount) AS kjoule,SUM( n.Kohlehydrate * d.amount ) AS kohlehydrate " +
                "From dishes as d inner join nutrition as n on d.nutrition_ID = n.ID inner join users AS u on d.user_ID = u.ID " +
                "Where d.user_id = (?) " +
                "GROUP BY TIMESTAMP " +
                "ORDER BY stat_day";

        Connection dbConn = new MySQL().getConnection();

        // Prepared Statement, welches auf die UserId zugreift wurde nicht implementiert.
        PreparedStatement prep = dbConn.prepareStatement(sql);
        prep.setInt(1, userID);

        ResultSet rs = prep.executeQuery();

        while (rs.next()) {
            DishStats auswertungsEintrag = new DishStats(rs.getDate("stat_day"),
                    rs.getDouble("eiweiß"),
                    rs.getDouble("fett"),
                    rs.getDouble("kalorien"),
                    rs.getDouble("kjoule"),
                    rs.getDouble("kohlehydrate")

            );
            auswertung.add(auswertungsEintrag);
        }
    }

    public String getHTMLFromDishTable() throws SQLException, ClassNotFoundException {
        this.getStats(userID);
        String html = "";
        for (DishStats data : auswertung) {
            html += "<tr>" + data.toTableRow() + "</tr>";
        }

        return html;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

}
