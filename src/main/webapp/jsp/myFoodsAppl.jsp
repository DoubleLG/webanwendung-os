<%@page import="java.sql.SQLException" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="java.util.Date" %>
<%@page import="java.util.Locale" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Insert title here</title>
</head>
<body>
<jsp:useBean id="user" class="de.hs_lu.beans.UserBean" scope="session"/>
<jsp:useBean id="dish" class="de.hs_lu.beans.DishBean" scope="session"/>

<%-- Hier wird der �bergebene String auf ungleich null gepr�ft--%>
<%!

    public String denullify(String s) {
        return (s == null) ? "" : s;
    }


%>

<%
    String saveDish = this.denullify(request.getParameter("saveDish"));
    String[] foods = request.getParameterValues("foodID");
    String[] amounts = request.getParameterValues("amount");
    int userId = 0;
    try {
        userId = user.getIdentifierSQL();
    } catch (SQLException e) {
        e.printStackTrace();
    } catch (ClassNotFoundException e) {
        e.printStackTrace();
    }
    Date timestamp = new SimpleDateFormat("yyyy-MM-dd", Locale.GERMANY).parse(request.getParameter("currentDate"));


    if (saveDish.equals("save")) {
        try {
            dish.saveDish(foods, amounts, userId, timestamp);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    } else {
        response.sendRedirect("./loginView.jsp");

    }

    response.sendRedirect("./myAnalyticsView.jsp");


%>

</body>
</html>